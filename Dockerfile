FROM python:3

ADD main.py /

RUN pip3 install matrix_client
RUN pip3 install matrix_bot_api
RUN pip3 install pandas

CMD [ "python3", "./main.py" ]