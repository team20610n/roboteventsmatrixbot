#!/usr/bin/env python3
import random

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler

import json
import requests
import pandas as pd
from time import sleep

# import urllib.request
# import sys
# import operator
# import iso8601
# from dateutil import tz
# from datetime import datetime


missingDataMessage='Missing required data. To get a list of commands !help'
defaultErrorMessage=" Please check your input and try again.\nIf the problem persists please file a issue at\n https://gitlab.com/team-20610n/roboteventsmatrixbot."
activeEvent = {}

# Send a raw URL ender and get back a python dict with data
def getRBEData(query):
    with open("RobotEventsKey", 'r') as APIKey:
        endpoint = "https://www.robotevents.com/api/v2/" + query
        print("requesting: " + endpoint)
        ROBOTEVENTS_HEADER = {"Authorization": "Bearer "+APIKey.read(), "accept": "application/json"}
        df_retval = None
        response = requests.get(endpoint,headers=ROBOTEVENTS_HEADER)
        json_results = json.loads(response.content.decode('utf-8'))
        try:
            df_retval = pd.read_json(json.dumps(json_results['data']))
        except KeyError:
            raise Exception("malformed request!")

        while json_results['meta']['next_page_url'] is not None:
            print(json_results['meta']['next_page_url'])
            response = requests.get(json_results['meta']['next_page_url'],headers=ROBOTEVENTS_HEADER)
            json_results = json.loads(response.content.decode('utf-8'))
            df_thispage = pd.read_json(json.dumps(json_results['data']))
            df_retval = df_retval.append(df_thispage)

        new_index = []
        for i in range(0, int(json_results['meta']['total'])):
            print("Grabing aditional pages " + str(i))
            new_index.append(i)
        df_retval.index = new_index
        df_retval = df_retval.to_dict()

        return df_retval

# Always gets the current seasons ID. NEEDS REDONE
def getCurrentSeasonID():
    try:
        return getRBEData('seasons?program%5B%5D=1&active=true')['id'][0]
    except:
        return None

def getEventTeams(ID):
    try:
        return getRBEData("events/" + str(ID) + "/teams")
    except:
        return None

# Get general info in a team.
def getTeamData(number):
    try:
        return getRBEData("teams?number%5B%5D=" + str(number))
    except:
        return None

# Get a team's internal ID.
def getTeamID(number):
    try:
        teamData = getTeamData(number)['id']
        return teamData
    except:
        return None

# Get the team's active season ID.
def getSeasonID(number):
    seasonID = {}
    teamID = getTeamID(number)

    try:
        for i in range(len(teamID)):
            data = (getRBEData("seasons?team%5B%5D=" + str(teamID[i])))
            if (data != {}):
                seasonID[i] = data['id'][0]
        
        return seasonID
    except TypeError:
        print("getSeasonID ERROR: TypeError" + defaultErrorMessage)
        print(teamID)
        print(seasonID)
        return None

# Get current skills data about a team.
def getSkillsData(number):
    skillsData = {}
    try:
        teamID = getTeamID(number)
        seasonID = getSeasonID(number)

        for i in range(len(seasonID)):
            print('teams/' + str(teamID[i]) + '/skills?season%5B%5D=' + str(seasonID[i]))
            skillsData[i] = getRBEData('teams/' + str(teamID[i]) + '/skills?season%5B%5D=' + str(seasonID[i]))

        return skillsData
    except TypeError:
        print("getSkillsData ERROR: TypeError" + defaultErrorMessage)
        print(teamID)
        print(seasonID)
        print(skillsData)
        return None

# Get a team's best skills run.
def getSkillsRank(number):
    try:
        data = getSkillsData(number)
        # print(data)
        print("Got data")
        result = ''
        # len(data)
        for team in range(len(data)):
            print(getTeamData(number)['registered'][team])
            if (getTeamData(number)['registered'][team] == True):
                if (team > 0):
                    result += '\n\n'
                driverScores = {data[team]['event'][0]['id']: 0}
                programmingScores = {data[team]['event'][0]['id']: 0}
                eventIDs = {0: data[team]['event'][0]['id']}

                # loops through each match for the given team
                for runCount in range(len(data[team]['id'])):
                    print("testing runCount " + str(runCount) + " on team " + str(team))
                    # Adds unique event ids to the eventIDs dictionary
                    for i in range(len(eventIDs)):
                        print("Getting unique event IDs " + str(i))
                        if (eventIDs[i] != data[team]['event'][runCount]['id']):
                            eventIDs[i+1] = data[team]['event'][runCount]['id']
                            programmingScores[ data[team]['event'][runCount]['id'] ] = 0
                            driverScores[ data[team]['event'][runCount]['id'] ] = 0
                
                for runCount in range(len(data[team]['id'])):
                    print("Getting high scores " + str(runCount))
                    if (data[team]['type'][runCount] == 'programming' and data[team]['score'][runCount] > programmingScores[ data[team]['event'][runCount]['id'] ]):
                        programmingScores[ data[team]['event'][runCount]['id'] ] = data[team]['score'][runCount]

                    elif (data[team]['type'][runCount] == 'driver' and data[team]['score'][runCount] > driverScores[ data[team]['event'][runCount]['id'] ]):
                        driverScores[ data[team]['event'][runCount]['id'] ] = data[team]['score'][runCount]

                totalScores = 0
                bestEvent = 0
                for i in range(len(eventIDs)):
                    print("Looped: " + str(i))
                    print(programmingScores[ eventIDs[i] ] + driverScores[ eventIDs[i] ])
                    if ((programmingScores[ eventIDs[i] ] + driverScores[ eventIDs[i] ]) >= totalScores):
                        totalScores = (programmingScores[ eventIDs[i] ] + driverScores[ eventIDs[i] ])
                        bestEvent = eventIDs[i]

                # result += 'RESULT:  ' + str(programmingScores[ eventIDs[i] ]) + "  " + str(driverScores[ eventIDs[i] ]) + "\n"
            
                result += ('ID: ' + str(data[team]['team'][i]['id']) + '  Number: ' + str(data[team]['team'][i]['name'])
                    + '\nEvent Rank: ' + str(data[team]['rank'][i])
                    + '\nAuton Score: ' + str(programmingScores[bestEvent])
                    + '\nDrive Score: ' + str(driverScores[bestEvent])
                    + '\nTotal: ' + str(programmingScores[bestEvent] + driverScores[bestEvent]))

                return result
    except KeyError:
        return "getSkillsRank ERROR: KeyError" + defaultErrorMessage

    except TypeError:
        return "getSkillsRank ERROR: TypeError" + defaultErrorMessage


def getEvents(string):
    try:
        print("Getting event: " + str(string))
        if ("-" in string):
            print("SKU detected")
            data = getRBEData("events?sku%5B%5D=" + str(string))
            return str(data['name'][0] + '\n' + "ID: " + str(data['id'][0]) + "  SKU: " + str(data['sku'][0]) + "\n")
        else:
            print("Team number detected")
            data = getRBEData("events?team%5B%5D=" + str(getTeamID(string)[0]) + "&season%5B%5D=" + str(getSeasonID(string)[0]))
            print(data)
            results = ''
            for i in range(0, len(data['id'])):
                results += data['name'][i] + '\n' + "ID: " + str(data['id'][i]) + "  SKU: " + str(data['sku'][i]) + "\n"
            return str(results)
    except:
        return "I can't find data on " + str(string)
    

# def getTeamsMatches(number, event):
#     teamsurl = 'https://api.vexdb.io/v1/get_matches?team='+ str(number) +'&sku=' + str(event)
#     teamsresponse = urllib.request.urlopen(teamsurl)
#     teamsdata = teamsresponse.read()
#     teamstext = teamsdata.decode('utf-8')
#     teamsy = json.loads(teamstext)
#     teamsdata = json.loads(teamstext)["result"]
#     response = ""
#     for i in range(0, int(teamsy["size"])):
#         if (teamsdata[i]["scheduled"] != '1970-01-01T00:00:00+00:00'):
#             utc = iso8601.parse_date(str(teamsdata[i]["scheduled"]))
#             utc = utc.replace(tzinfo=tz.gettz('UTC'))
#             central = utc.astimezone(tz.tzlocal())

#             datetime.time(central)
#             if (datetime.time(central).hour > 12):
#                 matchTime=(str(datetime.time(central).hour-12)) + ":" + str(datetime.time(central).minute) + " PM"
#             else:
#                 matchTime=(str(datetime.time(central).hour)) + ":" + str(datetime.time(central).minute) + " AM"

#             response += ("Match Number *" + str(teamsdata[i]["matchnum"]) + "*  " + matchTime + '  ' + str(teamsdata[i]["field"]) + "\n"
#                         "Blue: " + str(teamsdata[i]["blue1"])+ '   ' + str(teamsdata[i]["blue2"]) + '  ' + str(teamsdata[i]["bluescore"]) + "\n" +
#                         "Red:  " + str(teamsdata[i]["red1"])+ '   ' + str(teamsdata[i]["red2"]) + '  ' + str(teamsdata[i]["redscore"]) + "\n")
#         else:
#             response += ("Turnament match: " + str(teamsdata[i]["round"]) + "  " + str(teamsdata[i]["field"]) + "\n"
#                         "Blue: " + str(teamsdata[i]["blue1"])+ '   ' + str(teamsdata[i]["blue2"]) + '  ' + str(teamsdata[i]["bluescore"]) + "\n" +
#                         "Red:  " + str(teamsdata[i]["red1"])+ '   ' + str(teamsdata[i]["red2"]) + '  ' + str(teamsdata[i]["redscore"]) + "\n")
#     return response.replace(number.upper(), "*"+str(number.upper())+"*")

# def getRankings(event):
#     teamNames = {}
#     teamsurl = 'https://api.vexdb.io/v1/get_teams?sku=' + str(event)
#     teamsresponse = urllib.request.urlopen(teamsurl)
#     teamsdata = teamsresponse.read()
#     teamstext = teamsdata.decode('utf-8')
#     teamsy = json.loads(teamstext)
#     teamsdata = json.loads(teamstext)["result"]

#     rankingsurl = 'https://api.vexdb.io/v1/get_rankings?sku=' + str(event)
#     rankingsresponse = urllib.request.urlopen(rankingsurl)
#     rankingsdata = rankingsresponse.read()
#     rankingstext = rankingsdata.decode('utf-8')
#     rankingsy = json.loads(rankingstext)
#     rankingsdata = json.loads(rankingstext)["result"]
#     response = ""

#     for i in range(0, int(teamsy["size"])):
#         teamNames[str(teamsdata[i]["number"])] = str(teamsdata[i]["team_name"])

#     for i in range(0, int(rankingsy["size"])):
#                 try:
#                     response += (str(rankingsdata[i]["rank"]) + ": " +str(rankingsdata[i]["team"]) + "  " + teamNames[str(rankingsdata[i]["team"])] + "\n")
#                 except:
#                     pass
#     return response

# def getSkillsRankings(event):
#     teamNames = {}
#     teamsurl = 'https://api.vexdb.io/v1/get_teams?sku=' + str(event)
#     teamsresponse = urllib.request.urlopen(teamsurl)
#     teamsdata = teamsresponse.read()
#     teamstext = teamsdata.decode('utf-8')
#     teamsy = json.loads(teamstext)
#     teamsdata = json.loads(teamstext)["result"]

#     rankingsurl = 'https://api.vexdb.io/v1/get_skills?type=2&sku=' + str(event)
#     rankingsresponse = urllib.request.urlopen(rankingsurl)
#     rankingsdata = rankingsresponse.read()
#     rankingstext = rankingsdata.decode('utf-8')
#     rankingsy = json.loads(rankingstext)
#     rankingsdata = json.loads(rankingstext)["result"]
#     response = ""

#     for i in range(0, int(teamsy["size"])):
#         teamNames[str(teamsdata[i]["number"])] = str(teamsdata[i]["team_name"])

#     for i in range(0, int(rankingsy["size"])):
#                 try:
#                     response += (str(rankingsdata[i]["rank"]) + ": " + str(rankingsdata[i]["score"]) + "   " + str(rankingsdata[i]["team"]) + "  " + teamNames[str(rankingsdata[i]["team"])] + "\n")
#                 except:
#                     pass
#     return response
    

# Global variables
USERNAME = ""  # Bot's username
PASSWORD = ""  # Bot's password
SERVER = ""  # Matrix server URL

with open("MatrixUserPass", 'r') as keys:
    USERNAME = keys.readline().split("=")[1].replace("\n", "")
    PASSWORD = keys.readline().split("=")[1].replace("\n", "")
    SERVER = keys.readline().split("=")[1].replace("\n", "")


def hi_callback(room, event):
    # Somebody said hi, let's say Hi back
    room.send_text("Hello, " + event['sender'])

def test_callback(room, event):
    # Somebody said hi, let's say Hi back
    room.send_text("It works!!")


def help_callback(room, event):
    room.send_text("Robot Events Bot help\n"
                    "!help | shows this help\n"
                    "!calc skills [red's score] [blue's score] | calculates skills score from field score\n"
                    "!get teams | lists all teams at the active event\n"
                    "!get team [team number] | info about a team\n"
                    "!get skills [team number] | give teams highest skills rank and score\n"
                    "!list events [SKU or team number] | lists all events for a city\n"
                    "!set event [event ID] | sets the event \n"
                    "!get event | gets the current event\n"
                    #"!list matches [team number] | gets all matches for the given team (NOT IMPLEMENTED YET!)\n"
                    #"!get rankings | gets the current ranking for the active event(NOT IMPLEMENTED YET!)\n"
                    "!list skills | gets the current skills standings for the active event"
                    )


def dieroll_callback(room, event):
    # someone wants a random number
    args = event['content']['body'].split()

    # we only care about the first arg, which has the die
    die = args[0]
    die_max = die[2:]

    # ensure the die is a positive integer
    if not die_max.isdigit():
        room.send_text('{} is not a positive number!'.format(die_max))
        return

    # and ensure it's a reasonable size, to prevent bot abuse
    die_max = int(die_max)
    if die_max <= 1 or die_max >= 1000:
        room.send_text('dice must be between 1 and 1000!')
        return

    # finally, send the result back
    result = random.randrange(1,die_max+1)
    room.send_text(str(result))

# !calc skills [red's score] [blue's score]
def calc_skills_callback(room, event):
    try:
        args = event['content']['body'].split()
        room.send_text("Score: " + str(int(args[2]) - int(args[3]) + 63))
    except:
        room.send_text(missingDataMessage)

def set_event_callback(room, event):
    args = event['content']['body'].split()
    activeEvent[event['room_id']] = str(args[2])
    room.send_text("Set event ID to  " + str(args[2]))

def get_event_callback(room, event):
    try:
        room.send_text("Current event ID is  " + str(activeEvent[event['room_id']]))
    except:
        room.send_text("Bad event code. Run !set event [ID]")


def list_events_callback(room, event):
    args = event['content']['body'].split()
    response = getEvents(str(args[2]).strip(" "))
    if (response != ''):
        room.send_text(response)
    else:
        room.send_text("No current events at that location.")


def get_team_callback(room, event):
    try:
        args = event['content']['body'].split()
        if (len(args) >= 3):
            response = ''
            data = getTeamData(str(args[2]))
            
            for i in range(0, len(data['id'])):
                if (data['registered'][i] == True):
                    if (i > 0):
                        response += '\n\n'
                    response += str(
                        data['program'][i]['code'] + ' ' + data['number'][i] + '\n' +
                        data['team_name'][i] + ' ' + str(data['robot_name'][i]) + '\n' +
                        data['location'][i]['city'] + ' ' + data['location'][i]['region'] + ' ' + data['location'][i]['country'] + '\n' +
                        data['grade'][i] + '  Active: ' + str(data['registered'][i]) + '\n' +
                        'ID: ' + str(data['id'][i]))
            
            room.send_text(response)
        else:
            response = ''
            data = getEventTeams(getRBEData("events?sku%5B%5D=" + str(activeEvent[event['room_id']]))['id'][0])
            for i in range(0, len(data['id'])):
                response += str(data['number'][i]) + " " + str(data['team_name'][i]) + " | " + str(data['location'][i]['city']) + " " + str(data['location'][i]['region']) + " | " + str(data['grade'][i]) + "\n"
            room.send_text(response)

    except:
        room.send_text('No team data.')


def get_skills_callback(room, event):
    args = event['content']['body'].split()
    response = getSkillsRank(str(args[2]))
    if (response != 'None'):
        room.send_text(str(response))
    else:
        room.send_text('No team data. Maybe you mixed the numbers up?')


# def list_matches_callback(room, event):
#     args = event['content']['body'].split()
#     try:
#         room.send_text(getTeamsMatches(str(args[2]), str(activeEvent[event['room_id']])))
#     except:
#         room.send_text(missingDataMessage)



def get_rankings_callback(room, event):
    pass


def list_skills_callback(room, event):
    print("Listing skills")
    try:
        data = getRBEData('events/' + str(getRBEData("events?sku%5B%5D=" + str(activeEvent[event['room_id']]))['id'][0]) + '/skills')
        print("got data")
        if (data == {}):
            room.send_text("I can't find data for that event. Try setting the event again.")
        dataCount = len(data['id'])
        programmingScores = {}
        drivingScores = {}
        teamNumber = {}

        for i in range(0, dataCount):
            if (data['type'][i] == 'driver'):
                drivingScores[data['rank'][i]] = str(data['score'][i])
                teamNumber[data['rank'][i]] = str(data['team'][i]['name'])

            elif (data['type'][i] == 'programming'):
                programmingScores[data['rank'][i]] = str(data['score'][i])

        outPutString = 'Skills rankings:'
        for i in range(1, len(drivingScores)+1):
            outPutString = (outPutString + '\n' +
            'Number: ' + str(teamNumber[i]) +
            ' | DS: ' + str(drivingScores[i]) + 
            ' | AS: ' + str(programmingScores[i]) + 
            ' | S: ' + str(int(drivingScores[i]) + int(programmingScores[i]))
            )
        
        room.send_text(outPutString)

    except IndexError:
        return 'No data.'
    except:
        return 'None'



def main():
    # Create an instance of the MatrixBotAPI
    bot = MatrixBotAPI(USERNAME, PASSWORD, SERVER)
    bot.handle_invite
    
    # hi keyword
    hi_handler = MRegexHandler("!hi", hi_callback)
    bot.add_handler(hi_handler)

    # calc skills keyword
    calc_skills_handler = MRegexHandler("!calc skill", calc_skills_callback)
    bot.add_handler(calc_skills_handler)

    # hi keyword
    set_event_handler = MRegexHandler("!set event", set_event_callback)
    bot.add_handler(set_event_handler)

    # test keyword
    test_handler = MRegexHandler("!test", test_callback)
    bot.add_handler(test_handler)
    
    # Help keyword
    help_handler = MRegexHandler("!help", help_callback)
    bot.add_handler(help_handler)

    # get event keyword
    get_event_handler = MRegexHandler("!get event", get_event_callback)
    bot.add_handler(get_event_handler)

    # list events keyword
    list_events_handler = MRegexHandler("!list event", list_events_callback)
    bot.add_handler(list_events_handler)

    # get team keyword
    get_team_handler = MRegexHandler("!get team", get_team_callback)
    bot.add_handler(get_team_handler)

    # get skills keyword
    get_skills_handler = MRegexHandler("!get skills", get_skills_callback)
    bot.add_handler(get_skills_handler)

    # list matches keyword
    # list_matches_handler = MRegexHandler("!list matches", list_matches_callback)
    # bot.add_handler(list_matches_handler)

    # get rankings keyword
    get_rankings_handler = MRegexHandler("!get rankings", get_rankings_callback)
    bot.add_handler(get_rankings_handler)

    # list skills keyword
    list_skills_handler = MRegexHandler("!list skills", list_skills_callback)
    bot.add_handler(list_skills_handler)


    # Start polling
    bot.start_polling()

    # print that the bot started properly
    print("SUCCESS: started and connected.")

    # Infinitely read stdin to stall main thread while the bot runs in other threads
    while True:
        sleep(999999999)


if __name__ == "__main__":
    try:
        while(True):
            main()
    except:
        print("ERROR: Cannot connect to server. RETRYING in 10 seconds...")
        sleep(10)