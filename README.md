## Features

- List team information including there region, name, and robot name.
- Set your current event to more quickly get at the data from that event.
- Multi romm support.
- Get a teams best skills run for the current season.
- Get skills rankings from an event.
- List events that a team are registered for.

## Install instructions

### Installation prerequisites

1. Matrix account on a matrix home server that allows boting.
2. A server that can run docker.
3. A RobotEvents.com v2 api key.

### Docker install commands

1. Put your RobotEvents key in the `RobotEventsKey` file.
2. Put your matrix login details in `MatrixUserPass` file.
3. `sudo docker build -t roboteventsmatrix Docker`
4. Don't forget to change the host file paths to were your conf files actually are.
5. `sudo docker run -v ~/RobotEventsMatrix/MatrixUserPass:/MatrixUserPass -v ~/RobotEventsMatrix/RobotEventsKey:/RobotEventsKey roboteventsmatrix:latest`
6. Invite the bot to an unencrypted matrix room.
7. type `!help` for a list of all avaliable commands.
